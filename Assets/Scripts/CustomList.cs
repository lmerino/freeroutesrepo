﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CustomList {
    private List<Transform> list;

    private bool isDirty;
    Transform[] array;
    public CustomList() {
        list = new List<Transform>();
    }

    public void Add(Transform t) {
        list.Add(t);
        isDirty = true;
    }

    public void Remove(Transform t) {
        list.Add(t);
        isDirty = true;
    }

    public void RemoveAt(int i) {
        list.RemoveAt(i);
        isDirty = true;
    }

    public int Count(){
        return list.Count;
    }

    public Transform[] ToArray() {
        if (isDirty) {
            array = list.ToArray();
            isDirty = false;
        }
        return array;
    }
}

