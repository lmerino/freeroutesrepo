﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FreeRouteManager : MonoBehaviour
{
    public PathFollower freeRoute;
    
    private void Start() {
        freeRoute = new PathFollower();
        //freeRoute.RouteClosed += new PathFollower.OnRouteClosed(OnRouteClosed);
    }

    void OnDrawGizmos() {
        //freeRoute.OnDrawGizmos();
    }

    private void Update() {
        //freeRoute.OnDrawGizmos();
        freeRoute.Update();
    }
}
