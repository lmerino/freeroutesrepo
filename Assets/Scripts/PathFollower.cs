﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using System.IO;

/// <summary>
/// iTweem functions use a Transform[] variable, but we need this to be of dynamic sice as the user will be editing the nodes in the training manager.
/// This function is to update the Transform[] controlPath variable from the List<Transform> controlPathList variable, to use the Transform[] variable 
/// updated each time a node is added or removed, and be able tu use the more efficient data structure that the list in our free route.
/// </summary>
/// TODO: If this works, try to do it with an event instead of a new type (ObservableCollection type seems useful, but does not implement ToArray())
/// https://stackoverflow.com/questions/12785019/listt-firing-event-on-change
/// List<Transform> myList = MyObservableColletion.ToList<Transform>().ToArray();
/// 

//public class CustomListV2<T> where T : Transform { //List<T> where T : Transform {
//    List<Transform> lst;

//    public CustomListV2(out Transform[] controlPathRef) {
//        lst = new List<Transform>();
//        controlPathRef = lst.ToArray(); //init
//    }

//    //update controlPath[] variable passed by ref when the list is modified. 
//    public void Add(T item, out Transform[] controlPathRef) {
//        lst.Add(item);
//        controlPathRef = lst.ToArray();
//    }
//}
/*
public class CustomListV3<T> : List<T> where T : Transform {

    Transform[] auxTransformList;
    public CustomListV3(ref Transform[] controlPathRef) {
        controlPathRef = this.ToArray(); //init
        //auxTransformList = controlPathRef;
        setRefEqual(ref auxTransformList, ref controlPathRef);
    }

    void setRefEqual(ref Transform[] ref1, ref Transform[] ref2) {
        ref1 = ref2;
    }

    //update controlPath[] variable passed by ref when the list is modified. 
    public new void Add(T item) {
        base.Add(item);
        pathListUpdate(ref auxTransformList);
    }

    private void pathListUpdate(ref Transform[] controlPath) {
        controlPath = this.ToArray();
    }
}
*/

public class PathFollower {

    //private Transform[] controlPath;
    //public List<Transform> controlPathList;
    //public CustomList controlPathList;

    private CustomList controlPath;

    private bool _pathReady;
    public Transform character;
    public enum Direction { Forward, Reverse };

    private float pathPosition = 0f;
    private RaycastHit hit;
    //private float speed = .09f;
    private float rayLength = 5;
    private Direction characterDirection;
    private Vector3 floorPosition;
    private float lookAheadAmount = .002f;

    # region route type variables

    public iTween.LoopType loopType;

    public enum FreeRouteType {
        straight,
        curved
    }

    public FreeRouteType straightOrCurved;
    # endregion

    #region movement parameter variables
    public float initialSpeedInKmh;
    public float limitSpeedInKmh;
    public bool forceVehicleSpeed;
    public float forcedSpeed;

    public float accelerationInms2;
    [Range(0, 1)]
    public float brake;
    private float pathLength;
    private float pathStraightRouteLenght;
    private float brakingDecelerationInms2 = -10.0f;

    //vectorial movement variables
    private Vector3 _characterSpeed = Vector3.zero;
    private Vector3 _characterAccel = Vector3.zero;
    private Vector3 _charactePos = Vector3.zero;
    private float _deltaPath;
    #endregion

    private bool _editionStatus;

    public bool editionStatus {
        get { return _editionStatus;}
        set {
            _editionStatus = value;
            if (!_editionStatus & controlPath.Count() >= 2){
                //OnRouteClosed();
                RouteClosed?.Invoke();
            }    
        } 
    }

    public delegate void InitialSetHandler();

    public event InitialSetHandler RouteClosed;

    private void OnRouteClosed(){
        pathLength = iTween.PathLength(controlPath.ToArray());
        pathStraightRouteLenght = iTween.PathStraightRouteLength(controlPath.ToArray());
        double[] partialLenghts = getPathPartialLengths(controlPath.ToArray());
    }

    public void OnDrawGizmos() {
        //if (!_pathReady) return;
        //if (straightOrCurved == FreeRouteType.curved)
        //    iTween.DrawPath(controlPath.ToArray(), Color.blue);
        //else if (straightOrCurved == FreeRouteType.straight)
        //    iTween.DrawStraightPath(controlPath.ToArray(), Color.blue);
    }


    //Keep it in case the class needs to derive from monobehaviour. Erase otherwise
    //void Start() {
    //    //plop the character pieces in the "Ignore Raycast" layer so we don't have false raycast data:    

    //    character = this.transform;

    //    foreach (Transform child in character) {
    //        child.gameObject.layer = 2;
    //    }
    //    _characterSpeed = initialSpeedInKmh * character.forward;
    //    controlPath = new CustomList();
    //    addNodesToListByName();
    //    pathLength = iTween.PathLength(controlPath.ToArray());
    //    pathStraightRouteLenght = iTween.PathStraightRouteLength(controlPath.ToArray());
        
    //    updateMovementParams();
    //}

    public PathFollower(){
        //set the character that will follow the route    
        //character = this.transform;
        character = GameObject.Find("Cube").transform;
        //plop the character pieces in the "Ignore Raycast" layer so we don't have false raycast data:
        foreach (Transform child in character) {
            child.gameObject.layer = 2;
        }
        _characterSpeed = initialSpeedInKmh * character.forward;
        
        controlPath = new CustomList();
        RouteClosed += OnRouteClosed;//add listener. I do not listener for the moment. In the proyect I will call the method directly for the moment. 
        addNodesToListByName();

        //need at least 2 nodes to obtain the lengths
        //pathLength = iTween.PathLength(controlPath.ToArray());
        //pathStraightRouteLenght = iTween.PathStraightRouteLength(controlPath.ToArray());
           

        //default movement params
        forceVehicleSpeed = true;
        forcedSpeed = 10f;
        straightOrCurved = FreeRouteType.curved;

        updateMovementParams();
    }

    private void addNodesToListByName(){
        for (int i = 1; i <= 15; i++){
            string auxStr = "Node" + i.ToString();
            GameObject go = GameObject.Find(auxStr);
            controlPath.Add(go.transform);
        }
        //controlPath = controlPathList.auxTransformList;
        //close route
        editionStatus = false;
        _pathReady = true;
    }


    //trial. Does not work because every 2 nodes the path is the straight path.
    //first attempt. I leave it commented. To be erades in the future
    private double[] getPathPartialLengths(Transform[] path) {
        double[] partialLenghts = new double[path.Length - 1];
        for (int i = 0; i < path.Length-1; i++) {
            Transform[] partialPath = new Transform[2] { path[i], path[i+1] };
            partialLenghts[i] = iTween.PathLength(partialPath);
        }
        double totalLenghtCheck = 0f;
        for (int i = 0;  i < partialLenghts.Length; i++) {
            totalLenghtCheck += partialLenghts[i];
        }
        if (totalLenghtCheck == iTween.PathLength(path))
            Debug.LogError("fock hell. this is focking awesome");

        double totalStraightLength = iTween.PathStraightRouteLength(path);
        return partialLenghts;
    }

    private Vector3 getPointOnPath(Transform[] path, float percent, FreeRouteType straightOrCurved){
        Vector3 point = new Vector3();
        if (straightOrCurved == FreeRouteType.straight)
            point = iTween.PointOnStraightPath(path, percent);
        else if (straightOrCurved == FreeRouteType.curved)
            point = iTween.PointOnPath(path, percent);
        return point;
    }

    private void updateMovementParams(){
        //acceleration
        if (brake == 0 && !forceVehicleSpeed) {
            _characterAccel = character.forward * accelerationInms2;
        }
        if (brake != 0 && !forceVehicleSpeed)
            if (_characterSpeed.magnitude > 0.1f)
                _characterAccel = character.forward * brakingDecelerationInms2 * brake;
            else {
                _characterAccel = Vector3.zero;
                _characterSpeed = Vector3.zero;
            }

        //speed 
        _characterSpeed = character.forward * _characterSpeed.magnitude;//update speed direction
        if (forceVehicleSpeed)
            _characterSpeed = character.forward * forcedSpeed;
        else if (_characterSpeed.magnitude < limitSpeedInKmh || brake != 0)
            _characterSpeed += _characterAccel * Time.deltaTime;

        //position update for the next frame
        //_charactePos += _characterSpeed * Time.deltaTime; //pos will be determined with the speed, taking into account delta% correction

        //deltaPath correction
        float pathPercentsPerSecond = forceVehicleSpeed ? forcedSpeed / pathLength : _characterSpeed.magnitude / pathLength;
        //Debug.LogError(_characterSpeed.ToString());
        //Debug.LogError(_characterSpeed.magnitude.ToString());
        //Debug.LogError(Time.deltaTime.ToString());
        float pathVelocity = pathLength * pathPercentsPerSecond;
        float deltaPercent = pathPercentsPerSecond * Time.deltaTime;
        //Debug.LogError(deltaPercent.ToString());
        Vector3 nextPoint = getPointOnPath(controlPath.ToArray(), pathPosition + deltaPercent, straightOrCurved);
        float calcDist = Vector3.Magnitude(nextPoint - character.position);
        //Debug.LogError(calcDist.ToString());
        float calcVelocity = calcDist / Time.deltaTime;
        float velocityScale = pathVelocity / calcVelocity;

        //update pathposition
        //_deltaPath = (_characterSpeed * Time.deltaTime).magnitude / iTween.PathLength(controlPath);
        //delta path with velocity scale correction
        _deltaPath = deltaPercent * velocityScale;

        // For looping
        if (loopType == iTween.LoopType.loop){
            if (pathPosition > 1.0f)
                pathPosition -= 1.0f;
            else if (pathPosition < 0)
                pathPosition += 1.0f;
        }
        else if (loopType == iTween.LoopType.none) {
            if (pathPosition > 1.0f)
                pathPosition = 1.0f;
        }
        //TODO:ping-pong case

        pathPosition += _deltaPath;
        iTween.PutOnPath(character, controlPath.ToArray(), pathPosition);
        //Debug.Log(pathPosition.ToString());
    }

    public void Update() {
        updateMovementParams();
        //FindFloorAndRotation();
        //MoveCharacter();
    }

    //void FindFloorAndRotation() {
    //    float pathPercent = pathPosition % 1;
    //    Vector3 coordinateOnPath = getPointOnPath(controlPath.ToArray(), pathPercent, straightOrCurved);
    //    Vector3 lookTarget;

    //    //calculate look data if we aren't going to be looking beyond the extents of the path:
    //    if (pathPercent - lookAheadAmount >= 0 && pathPercent + lookAheadAmount <= 1) {

    //        //leading or trailing point so we can have something to look at:
    //        if (characterDirection == Direction.Forward) {
    //            lookTarget = getPointOnPath(controlPath.ToArray(), pathPercent + lookAheadAmount, straightOrCurved);
    //        }
    //        else {
    //            lookTarget = getPointOnPath(controlPath.ToArray(), pathPercent - lookAheadAmount, straightOrCurved);
    //        }

    //        //look:
    //        character.LookAt(lookTarget);

    //        //nullify rest of the rotation and look to next target only in the Y axis
    //        //float yRot = character.eulerAngles.y;
    //        //character.eulerAngles = new Vector3(0, yRot, 0);
    //    }
    //    float safetyUndergroundCoef = 1f;
    //    LayerMask layerMask = 1 << 0;
    //    if (Physics.Raycast(coordinateOnPath, -Vector3.up, out hit, rayLength, layerMask)) {
    //        floorPosition = hit.point;
    //    }
    //    //safety distance in case the coordinate path goes underground.Raycashit wont hit to the tarrain collider from below
    //    else if (Physics.Raycast(coordinateOnPath + Vector3.up * safetyUndergroundCoef, -Vector3.up, out hit, rayLength, layerMask)) {
    //        Debug.DrawRay(coordinateOnPath, -Vector3.up * hit.distance);
    //        floorPosition = hit.point;
    //    }
    //}

    //void MoveCharacter() {
    //    character.position = new Vector3(floorPosition.x, floorPosition.y, floorPosition.z);
    //}
}
