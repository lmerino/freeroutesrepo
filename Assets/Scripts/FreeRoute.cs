﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FreeRoute : MonoBehaviour
{
    public List<GameObject> points;
    public float speed;

    private void Start() { 
        StartCoroutine(followRoute());
    }

    private IEnumerator followRoute() {
        Vector3 absDepartPos = gameObject.transform.position;
        points.Insert(0, this.gameObject); //add self go pos to list soas to set the beginning of the route

        //set the end of the route, and the route as circular 
        GameObject initialPosAuxGO = new GameObject();
        initialPosAuxGO.transform.position = this.transform.position;
        points.Add(initialPosAuxGO);

        for (int i = 1; i < points.Count; i++) {
            Vector3 departurePos = points[i - 1].transform.position;
            Vector3 destinationPos = points[i].transform.position;
            Vector3 dir = Vector3.Normalize(destinationPos - departurePos);
            //float distance = (destinationPos - departurePos).magnitude;
            float sqrDistance = (destinationPos - departurePos).sqrMagnitude;
            float sqrTraveledDist;
            do {
                transform.position += dir * speed/10 * Time.deltaTime;
                sqrTraveledDist = (transform.position - departurePos).sqrMagnitude;
                yield return new WaitForEndOfFrame(); //for the object to move to the delta time speed
            } while (sqrTraveledDist < sqrDistance);
            //match final positions
            this.transform.position = destinationPos;
        }
    }
}
